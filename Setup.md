<!-- TOC ignore:true --> 
Squats & Fitness Pvt Ltd


**Installation and Deployment Guide for Dev environment (Ubuntu 20)**


<table>
  <tr>
   <td><strong>Author</strong>
   </td>
   <td>Nagapandi
   </td>
  </tr>
  <tr>
   <td><strong>Edited by</strong>
   </td>
   <td>Lamtei Wahlang
   </td>
  </tr>
  <tr>
   <td><strong>Document Version</strong>
   </td>
   <td>v1.1
   </td>
  </tr>
  <tr>
   <td><strong>Revised Date</strong>
   </td>
   <td>17.12.2021
   </td>
  </tr>
</table>




**<span style="text-decoration:underline;">Table of Contents</span>**


[TOC]



## 1. Ejabberd Server

Install Ejabberd in both the servers. Follow the procedures mentioned in this section to install Ejabberd in both servers


### 1.1. Installation Steps

Follow the below steps to install Ejabberd Server 


#### 1.1.1. Update Ubuntu & install erlang dependencies


```
$ sudo apt update
```



#### 1.1.2. Install Erlang 

Install Erlang version = 22

Check if erlang package is available
```
$ sudo apt list -a erlang
```

In case the version of Erlang is not available you can download the version directly and install. For more information, you can visit [here](https://www.erlang-solutions.com/downloads/). MirrorFly documentation for Erlang installation can be found [here](https://docs-internal.mirrorfly.com/docs/ejabberd-setup#erlang---installation-in-ubuntu-2004).

```
sudo code /etc/apt/sources.list
```
Add the following erlang package list to the source list

```
deb https://packages.erlang-solutions.com/ubuntu focal contrib
```
**Please use the appropriate distribution code name, here we are using focal (Focal Fossa - 20.0x LTS)**

```
$code /etc/apt/sources.list
```

Add the Erlang Solutions public key for "apt-secure" using following commands:
```
$wget https://packages.erlang-solutions.com/ubuntu/erlang_solutions.asc
$sudo apt-key add erlang_solutions.asc
```

Install Erlang 22
```
sudo apt-get install esl-erlang=1:22.3.4.9-1
```

**Upgrading Erlang: Please purge ebin and deps directories**
```
cd /opt/ejabberd/source/ejabberd
rm -rf ebin/*
rm -rf deps _build
```

#### 1.1.3. Validate Erlang installation


```
$ erl
Erlang/OTP 22 [erts-10.7.2.3] [source] [64-bit] [smp:8:8] [ds:8:8:10] [async-threads:1] [hipe]

Eshell V10.7.2.3  (abort with ^G)
1> 
```



#### 1.1.4. Create Ejabberd user and install ejabberd


```
# su - root
$ useradd -m -d /opt/ejabberd ejabberd
$ su - ejabberd
$ mkdir source 
$ cd source
$ git clone git@bitbucket.org:lamtei-wahlang/dmf0420pd017-squatsfitness-instantmessenger-ejabberd-19.05.git -b develop ejabberd
$ cd ejabberd
```

**Note:** Update the git username and check for the git branch accordingly to check out

```
$ git branch
$ git checkout develop
```

**Compile and build ejabberd**

```
$ ./autogen.sh
$ ./configure --prefix=/opt/ejabberd -enable-mysql
$ cp xmpp_codec.spec deps/xmpp/specs/xmpp_codec.spec
$ cd deps/xmpp/
$ make spec
$ make
$ cd ../../
$ make
$ make install
```

**Additionally, you may need to install few headers required for the build**

```
$sudo apt-get install libyaml-dev libssl-dev
```

**Note:** If make error comes then try to rerun the following steps again:

```
$ cp xmpp_codec.spec deps/xmpp/specs/xmpp_codec.spec
$ cd deps/xmpp/
$ make spec
$ make
$ cd ../../
$ make
$ make install
```


If make install still throws an error then follow the steps below and run **make install** again
```
vi deps/rabbit_common/ebin/rabbit_common.app
        change {vsn,[]} {vsn} )
```

**Note:** In case of dependency issues reported by rebar, please rerun the following before recompiling the project:
```
cd /opt/ejabberd/source/ejabberd
rm -rf ebin/*
rm -rf deps _build
```

**Note:** For permission issues, you may need to change ownership of the file/directory to ejabberd if not owned by ejabberd

#### 1.1.5. Configure ejabberd yml file 

Change the following configurations in **/opt/ejabberd/etc/ejabberd/ejabberd.yml** file

Updated configuration to enable bosh (mod_bosh) and websocket for testing.

##### 1.1.5.1. Please update the hosts, mysql and rabbitmq credentials


```
###
###              ejabberd configuration file
###
### The parameters used in this configuration file are explained at
###
###       https://docs.ejabberd.im/admin/configuration
###
### The configuration file is written in YAML.
### *******************************************************
### *******           !!! WARNING !!!               *******
### *******     YAML IS INDENTATION SENSITIVE       *******
### ******* MAKE SURE YOU INDENT SECTIONS CORRECTLY *******
### *******************************************************
### Refer to http://en.wikipedia.org/wiki/YAML for the brief description.
### However, ejabberd treats different literals as different types:
###
### - unquoted or single-quoted strings. They are called "atoms".
###   Example: dog, 'Jupiter', '3.14159', YELLOW
###
### - numeric literals. Example: 3, -45.0, .0
###
### - quoted or folded strings.
###   Examples of quoted string: "Lizzard", "orange".
###   Example of folded string:
###   > Art thou not Romeo,
###     and a Montague?
###

hosts:
  - "chat-sit3.fittr.com"

loglevel: 5
log_rotate_size: 10485760
log_rotate_date: ""
log_rotate_count: 1
log_rate_limit: 100

certfiles:
  - "/etc/letsencrypt/live/localhost/fullchain.pem"
  - "/etc/letsencrypt/live/localhost/privkey.pem"

listen:
  -
    port: 5222
    ip: "::"
    module: ejabberd_c2s
    max_stanza_size: 262144
    shaper: c2s_shaper
    access: c2s
    starttls_required: true
  -
    port: 5269
    ip: "::"
    module: ejabberd_s2s_in
    max_stanza_size: 524288
  -
    port: 5443
    ip: "::"
    module: ejabberd_http
    tls: true
    request_handlers:
      "/admin": ejabberd_web_admin
      "/api": mod_http_api
      "/bosh": mod_bosh
      "/captcha": ejabberd_captcha
      "/upload": mod_http_upload
      "/ws": ejabberd_http_ws
  -
    port: 5280
    ip: "::"
    module: ejabberd_http
    request_handlers:
      "/admin": ejabberd_web_admin
      "/api": mod_http_api
      "/oauth": ejabberd_oauth
      "/bosh": mod_bosh
      "/upload": mod_http_upload
      "/ws": ejabberd_http_ws
      "/captcha": ejabberd_captcha
  #-
  #  port: 1883
  #  ip: "::"
  #  module: mod_mqtt
  #  backlog: 1000

s2s_use_starttls: optional

auth_method: sql
auth_use_cache: false
oauth_db_type: sql

sql_type: mysql
sql_server: "localhost"
sql_database: "ejabberd_19"
sql_username: "chat_app_user"
sql_password: ""
sql_pool_size: 10
sql_connect_timeout: 1000

acl:
  admin:
    user:
      - "admin@chat-sit3.fittr.com"
  local:
    user_regexp: ""
  loopback:
    ip:
      - "127.0.0.0/8"
      - "::1/128"

access_rules:
  local:
    - allow: local
  c2s:
    - deny: blocked
    - allow
  announce:
    - allow: admin
  configure:
    - allow: admin
  muc_create:
    - allow: local
  pubsub_createnode:
    - allow: local
  trusted_network:
    - allow: loopback

api_permissions:
  "console commands":
    from:
      - ejabberd_ctl
    who: all
    what: "*"
  "admin access":
    who:
      - access:
          - allow:
            - acl: loopback
            - acl: admin
      - oauth:
        - scope: "ejabberd:admin"
        - access:
          - allow:
            - acl: loopback
            - acl: admin
    what:
      - "*"
      - "!stop"
      - "!start"
  "public commands":
    who:
      - ip: "127.0.0.1/8"
    what:
      - send_message
      - send_stanza_c2s
      - send_stanza
      - fly_send_message
      - add_channel
      - add_featured_channel
      - set_group_info
      - update_group_info
      - set_group_read_only_status
      - leave_group
      - remove_participant
      - add_participant
      - join_channel
      - add_rosteritem
      - send_rule_updated_notification      

commands_admin_access: configure
commands:
  - add_commands:
    - user
oauth_expire: 315360000
oauth_access: all

shaper:
  normal: 1000
  fast: 50000

shaper_rules:
  max_user_sessions: 10
  max_user_offline_messages:
    - 5000: admin
    - 100
  c2s_shaper:
    - none: admin
    - normal
  s2s_shaper: fast

registration_timeout: infinity
modules:
  mod_fittr: {}
  #mod_adhoc: {}
  mod_admin_extra: {}
  #mod_announce:
  #  access: announce
  #mod_avatar: {}
  mod_blocking: {}
  mod_bosh: {}
  #mod_caps: {}
  mod_carboncopy: {}
  mod_client_state: {}
  #mod_configure: {}
  #mod_disco: {}
  #mod_fail2ban: {}
  mod_http_api: {}
  #mod_http_upload:
  #  put_url: "https://@HOST@:5443/upload"
  mod_last:
    db_type: sql
    use_cache: false  
  mod_mam:
    ## Mnesia is limited to 2GB, better to use an SQL backend
    ## For small servers SQLite is a good fit and is very easy
    ## to configure. Uncomment this when you have SQL configured:
    db_type: sql
    assume_mam_usage: true
    default: always
  #mod_mqtt: {}
  #mod_muc:
  #  access:
  #    - allow
  #  access_admin:
  #    - allow: admin
  #  access_create: muc_create
  #  access_persistent: muc_create
  #  access_mam:
  #    - allow
  #  default_room_options:
  #    mam: true
  #mod_muc_admin: {}
  mod_offline:
    db_type: sql  
    access_max_user_messages: max_user_offline_messages
  mod_ping:
    send_pings: true
    ping_interval: 5
    ping_ack_timeout: 3
    timeout_action: none  
  mod_privacy:
    db_type: sql      
    use_cache: false  
  mod_private: {}
  #mod_proxy65:
  #  access: local
  #  max_connections: 5
  mod_mix:
    db_type: sql
  mod_mix_pam:  
    db_type: sql
    use_cache: false
  mod_pubsub:
    db_type: sql
    ignore_pep_from_offline: true
    last_item_cache: false
    access_createnode: pubsub_createnode
    plugins:
    #  - "flat"
      - "pep"
    force_node_config:
      ## Avoid buggy clients to make their bookmarks public
      "storage:bookmarks":
        access_model: whitelist
  #mod_push: {}
  #mod_push_keepalive: {}
  #mod_register:
    ## Only accept registration requests from the "trusted"
    ## network (see access_rules section above).
    ## Think twice before enabling registration from any
    ## address. See the Jabber SPAM Manifesto for details:
    ## https://github.com/ge0rg/jabber-spam-fighting-manifesto
    #ip_access: trusted_network
  mod_roster:
    db_type: sql
    versioning: true
    use_cache: false
  #mod_s2s_dialback: {}
  mod_shared_roster:
    db_type: sql      
  #mod_stream_mgmt:
  #  resend_on_timeout: if_offline
  mod_vcard:
    db_type: sql
    search: false    
    use_cache: false  
  mod_vcard_xupdate:
    use_cache: false    
  #mod_version:
  #  show_os: false

####################################
#### Custom modules from contus ####
####################################

  #fly_mod_stanza_ack: {}
  #fly_add_timestamp: {}
  #fly_mod_user_activities: {}
  #fly_mod_user_fav: {}
  #fly_mod_broadcast: {}
  #fly_mix_group_chat: {}
  #fly_mod_offline_push:
  #  rabbitmq_host: "127.0.0.1"
  #  rabbitmq_exchange: "fly_notification"
  #  rabbitmq_username: "chat_app_user"
  #  rabbitmq_password: "admin220"  
  #fly_mod_offline_web: {}
  #fly_mod_jitsi: {}

### Local Variables:
### mode: yaml
### End:
### vim: set filetype=yaml tabstop=8
```
**Note:** Configuration for certificates is not required in local setup, hence skipping this step 

##### 1.1.5.2. Ejabberd Port Configurations 

**Note**: If the below ports are changed in ejabberd.yml file then open ports according to that 


<table>
  <tr>
   <td>Port
   </td>
   <td>
    Details
   </td>
  </tr>
  <tr>
   <td>5222
   </td>
   <td>
    Standard port for Jabber/XMPP client connections, plain or STARTTLS.
   </td>
  </tr>
  <tr>
   <td>5280
   </td>
   <td>
    Standard port for Jabber/XMPP client connections for http connection
   </td>
  </tr>
</table>

##### 1.1.5.3. Testing ejabberd connection in web

Below is a quick testing code for web using [converse js](https://conversejs.org/).

Modify the index.html for the plugin initialization
```
converse.initialize({
    websocket_url: 'ws://localhost:5280/ws',
    play_sounds: true,
    jid: 'admin@chat-sit3.fittr.com',
    password: 'password',
    show_controlbox_by_default: true,
    debug: true       
});
```
Run following command on project folder
```
make serve
```
This will run converse with inbuilt http server at [http://localhost:8000](http://localhost:8000)

### 1.2. Start Ejabberd Service


#### 1.2.1. Start the ejabberd service and verify using following command


```
$ su - ejabberd
$ cd /sbin
$ ./ejabberdctl start
$ ./ejabberdctl status
The node ejabberd@localhost is started with status: started
ejabberd 19.0 is running in that node
```


**Note:**   
1. Start ejabberd service as ejabberd user
2. To start ejabberd service we need to install mysql & rabbitmq and enter  their credentials in ejabberd.yml file


#### 1.2.2. Start the ejabberd service and verify  using following command

To trobleshoot and debug ejabberd, run ejabberd in live mode or debug mode:

```
$ su - ejabberd
$ cd /sbin
$ ./ejabberdctl live 
OR
$ ./ejabberdctl debug
```

#### 1.2.3. Check the IP: 5222 from the browser it will show the XML like below


```
This XML file does not appear to have any style information associated with it. The document tree is shown below.
<stream:stream xmlns="jabber:client" xmlns:stream="http://etherx.jabber.org/streams" id="2453832808607366938" from="localhost" version="1.0">
<stream:error>
<xml-not-well-formed xmlns="urn:ietf:params:xml:ns:xmpp-streams"/>
</stream:error>
</stream:stream>
```



### 1.4. Error and Access Log

Ejabberd log files can be found in the following locations:

```
/opt/ejabberd/var/log/ejabberd/ejabberd.log and
/opt/ejabberd/var/log/ejabberd/error.log
```

### 1.5. For new code changes in ejabberd server you need to follow the below steps:

```
$ su - ejabberd
$ cd source/ejabberd
$ git pull
$ make
$ cp xmpp_codec.spec deps/xmpp/specs/xmpp_codec.spec
$ cd deps/xmpp/
$ make spec
$ make
$ cd ../../
$ make install
$ cd  ../..
$ sbin/ejabberdctl/restart
$ sbin/ejabberdctl/status
```



## 2. MySQL Server


Follow the procedures mentioned in this section to install MySQL Servers, Cluster Mode setup across multiple instances and for load balancing configuration.


### 2.1. MySQL Server Installation


#### 2.1.1. Prerequisites

Setup MySQL Server in Ubuntu 20.04


#### 2.1.2. Installation Steps

Follow the below steps to install MySQL Server.



Download MySQL packages and dependencies:

```
$ wget -c https://dev.mysql.com/get/Downloads/MySQL-8.0/libmysqlclient21_8.0.27-1ubuntu20.04_amd64.deb
$ wget -c https://dev.mysql.com/get/Downloads/MySQL-8.0/mysql-community-client-core_8.0.27-1ubuntu20.04_amd64.deb
$ wget -c https://dev.mysql.com/get/Downloads/MySQL-8.0/mysql-community-client_8.0.27-1ubuntu20.04_amd64.deb
$ wget -c https://dev.mysql.com/get/Downloads/MySQL-8.0/mysql-client_8.0.27-1ubuntu20.04_amd64.deb
$ wget -c https://dev.mysql.com/get/Downloads/MySQL-8.0/mysql-community-server-core_8.0.27-1ubuntu20.04_amd64.deb
$ wget -c https://dev.mysql.com/get/Downloads/MySQL-8.0/mysql-community-server_8.0.27-1ubuntu20.04_amd64.deb

$ sudo dpkg -i libmysqlclient21_8.0.27-1ubuntu20.04_amd64.deb 
$ sudo dpkg -i mysql-community-client-core_8.0.27-1ubuntu20.04_amd64.deb 
$ sudo dpkg -i mysql-community-client_8.0.27-1ubuntu20.04_amd64.deb 
$ sudo dpkg -i mysql-client_8.0.27-1ubuntu20.04_amd64.deb 
$ sudo apt install libmecab2
$ sudo dpkg -i mysql-community-server-core_8.0.27-1ubuntu20.04_amd64.deb 
$ sudo dpkg -i mysql-community-server_8.0.27-1ubuntu20.04_amd64.deb 
```


Starting MySQL

```
$ sudo systemctl start mysql
```

**systemctl doesn't display the outcome of all service management commands, so to be sure we succeeded, we'll use the following command:**

```
$ sudo systemctl status mysql
```

If MySQL has successfully started, the output should contain Active: active (running) and the final line should look something like:
Check the MySQL version using the following command.

```
$ mysql --version
mysql  Ver 8.0.27 for Linux on x86_64 (MySQL Community Server - GPL)
```

**Configuring MySQL**
MySQL includes a security script to change some of the less secure default options for things like remote root logins and sample users. Use this command to run the security script:
```        
$ sudo mysql_secure_installation
```

This will prompt you for the default root password. As soon as you enter it, you will be required to change it.
Output

Restart MySQL

```
sudo service mysql restart
```

#### 2.2 Uploading mysql schema:

Download the mysql schema from the below link,
[https://drive.google.com/drive/folders/1VCOlVcsq8tEjs5ovsC9bCHooy2iWNqKQ](https://drive.google.com/drive/folders/1VCOlVcsq8tEjs5ovsC9bCHooy2iWNqKQ) 

Now run the following command to push the schema into the DB,

Create mysql user for ejabberd and grant privileges: 
```
echo "GRANT ALL PRIVILEGES ON *.* TO 'chat_app_user'@'localhost' WITH GRANT OPTION;" | mysql -h localhost -u root -p
echo "CREATE DATABASE ejabberd_19;" | mysql -h localhost -u chat_app_user -p
$ mysqldump -u root -p ejabberd_19 < ejabberd_19.sql
```


#### 2.3. Running Mysql queries:

Run the following mysql queries inside  mysql command line:


```
INSERT INTO `chat_settings` VALUES (1,'@fittr.in','123456789','NO',40,60,60,1,1,'','','2020-04-23 12:06:32','2020-04-23 12:06:32','<ejabberd_domain_name>','https://<signal_domain_name>/','<ejabberd_domain_name>','<ejabberd_domain_name>','AIzaSyCdwzAZR6tx8KB-2dMn0KzSI1V0LpsYdH0','',5200,'NULL',31,32,2000,0,NULL,0,NULL,'https://<conference_domain_name>');
INSERT INTO chat_servers (config_id, url, username, password, is_turn) VALUES ("1", "stun:<stun/turn_domain_name>:3478", "", "", 0);
INSERT INTO chat_servers (config_id, url, username, password, is_turn) VALUES ("1", "turn:<stun/turn_domain_name>:3478", "<username>", "<password>", 1);
INSERT INTO users VALUES('123456789', '123456789', 'NULL', '', '', '0', '2020-04-23 12:07:43');

mysql> GRANT ALL PRIVILEGES ON *.* TO 'root'@'%'IDENTIFIED BY '<mysql_root_password>' WITH GRANT OPTION;
mysql> Flush privileges;
```



## 3. Install Redis server

### 3.1. Install redis

Run the following commands to install and configure redis


```
$ sudo apt update
$ sudo apt install redis-server
```



### 3.2 Configure redis

#### 3.2.1. Search for the following options, then change (or use) their default values according to your local environment needs in redis.conf file


```
vi /etc/redis/redis.conf
```

```
port       6379                #default port is 6379. 
daemonize  yes                 #run as a daemon
supervised systemd             #signal systemd
pidfile    /var/run/redis.pid  #specify pid file
loglevel   notice              #server verbosity level
logfile    /var/log/redis.log  #log file name
dir        /var/redis/         #redis directory
```

Test redis
```
$ sudo systemctl status redis
```


```
Change Bind address in the conf file to allow only  127.0.0.1 and your private IP address of the server.

```


#### 3.2.2. To set the password, edit redis.conf file, find the directive that reads requirepass and update the password:


```
requirepass foobared
```

### 3.3. Start redis service


```
$ systemctl start redis
$ systemctl enable redis
$ systemctl status redis
```



## 4. Docker Installation

Run the following command to install and start docker


```
$ sudo apt update && sudo apt upgrade
$ sudo apt install curl ca-certificates apt-transport-https software-properties-common
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
$ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
$ sudo apt update
$ apt-cache policy docker-ce
$ sudo apt install docker-ce
$ sudo systemctl status docker
```

### 4.1 Troubleshoot docker services

- List all docker services (including stopped services):
    ```
    $ sudo docker ps -a
    ```
- Stop docker service with container ID:
    ```
    $ sudo docker stop 2413b56c80e5
    ```
- To restart a service, you will have to remove existing container:
    ```
    $ sudo docker rm /fly-rabbit
    ```
- Then restart your service again:
    ```
    $ docker run -d --hostname fly-rabbit --name fly-rabbit -p 15672:15672 -p 5672:5672 -v "/var/log/rabbitmq:/var/log/rabbitmq" -v "/var/lib/rabbitmq:/var/lib/rabbitmq"  rabbitmq:3-management
    ```

In case the service does not start, this might be an error in starting the service itself. You can check the service logs using the following command:
```
sudo docker logs fly-rabbit
```


## 5. API Installation:

Run the following API installation commands in  both servers


### 5.1. Install RabbitMQ


```
$ docker run -d --hostname fly-rabbit --name fly-rabbit -p 15672:15672 -p 5672:5672 -v "/var/log/rabbitmq:/var/log/rabbitmq" -v "/var/lib/rabbitmq:/var/lib/rabbitmq"  rabbitmq:3-management
```

**Note: In case of issues with starting RabbitMQ, you can try the following**

Run docker in interactive mode to check the logs: 

```
sudo docker run --interactive --tty --hostname fly-rabbit --name fly-rabbit -p 15672:15672 -p 5672:5672 -v "/var/log/rabbitmq:/var/log/rabbitmq" -v "/var/lib/rabbitmq:/var/lib/rabbitmq"  rabbitmq:3-management
```
Create log file and add permission:

```
sudo vi /var/log/rabbitmq/rabbit@fly-rabbit_upgrade.log
chmod 777 /var/log/rabbitmq/rabbit@fly-rabbit_upgrade.log
```

Check erlang cookie permission, read permission should only be given to the owner. 
```
ls -ltra /var/lib/rabbitmq/
sudo chmod 400 /var/lib/rabbitmq/.erlang.cookie
```

Once run open in browser [http://localhost:15672](http://localhost:15672/) and login as **username** guest and **password** guest.


```
Once run open in browser http://ip-address:15672 and login as username guest and password guest
A. Choose admin and add admin users
B. Create exchanges:
   - api_notification
   - fly_notification
   - voip_notification
C. Create Queues 
   - android_call
   - android_message
   - api_call
   - api_message
   - email_queue
   - ios_call
   - ios_message
   - api_voip_call
   - profile
D. fly_notification - bind queues
   - android_call
   - android_message
   - ios_call
   - ios_message 
E. api_notification - bind queues 
   - api_call
   - api_message
   - api_email
F. voip_notification - api_voip_call
```



### 5.2. Deploy Java Services:


#### 5.2.1. Create docker bridge network 


```
$ sudo docker network create fittr -d bridge
```



#### 5.2.2. Config Service:

**Step 1 — Config yml Files**

Create yml files for corresponding services into folder** /opt/fittr/config**

**Step 2 — Create Docker image and run as docker container**


```
$ mkdir -p /opt/fittr/source
$ cd /opt/fittr/source
$ git clone git@bitbucket.org:community_developers/dmf0420pd017-squatsfitness-java-configservice.git -b develop configservice
$ cd configservice 
```


Change the following configurations in **src/main/resources/bootstrap.yml **file

please change search-locations and logging:->  file: as mentioned below :


```
server:
  port: 80
spring:
    profiles:
        active: native
    cloud:
        config:
           server:
                native:
                    search-locations: file:///opt/fittr/config/
logging:
  file: /var/logs/fittr/config.log
  pattern:
    console: '%d %-5level %logger : %msg%n'
    file: '%d %-5level [%thread] %logger : %msg%n'
  level:
   org.springframework.web: ERROR
   com.tcl: INFO
```



```
$mvn clean install
$docker build -t configservice .
$sudo docker run -d --name=configservice -e JAVA_OPTS='-Xmx64m -Xms64m' -v /var/logs/fittr/configservice/:/var/logs/fittr/configservice/ -v /opt/fittr/config/:/opt/fittr/config/  --network=fittr configservice
```


#### 5.2.3. Gateway Service:

Create *application.properties* file in resource folder and add details for the data source:

```
spring.datasource.url=jdbc:mysql://localhost/<your db name here>
spring.datasource.username=<your db username here>
spring.datasource.password=<your db password here>
spring.datasource.driver-class-name=com.mysql.jdbc.Driver
spring.jpa.database-platform=org.hibernate.dialect.MySQL5InnoDBDialect
```

Create Docker image and run as docker container

```
$ cd /opt/fittr/source
$ git clone git@bitbucket.org:community_developers/dmf0420pd017-squatsfitness-java-gatewayservice.git -b develop gatewayservice
$ cd gatewayservice && mvn clean install
$ docker build -t gatewayservice .
$ sudo docker run -d --name=gatewayservice -e JAVA_OPTS='-Xmx512m -Xmx512m' -v /var/logs/fittr/gatewayservice/:/var/logs/fittr/gatewayservice/ -v /opt/fittr/config/:/opt/fittr/config/ -p 8080:80 --network=fittr gatewayservice
```
 	 

#### 5.2.4. User Service:

Create *application.properties* file in resource folder and add details for the data source:

```
spring.datasource.url=jdbc:mysql://localhost/<your db name here>
spring.datasource.username=<your db username here>
spring.datasource.password=<your db password here>
spring.datasource.driver-class-name=com.mysql.jdbc.Driver
spring.jpa.database-platform=org.hibernate.dialect.MySQL5InnoDBDialect
```

Create Docker image and run as docker container

```
$ cd /opt/fittr/source
$ git clone git@bitbucket.org:community_developers/dmf0420pd017-squatsfitness-java-userservice.git -b develop userservice
$ cd userservice && mvn clean install
$ docker build -t userservice .
$ sudo docker run -d --name=userservice -e JAVA_OPTS='-Xmx512m -Xmx512m' -v /var/logs/fittr/userservice/:/var/logs/fittr/userservice/ -v /opt/fittr/config/:/opt/fittr/config/ --network=fittr userservice
```


#### 5.2.5. Contact Service:

Create *application.properties* file in resource folder and add details for the data source:

```
spring.datasource.url=jdbc:mysql://localhost/<your db name here>
spring.datasource.username=<your db username here>
spring.datasource.password=<your db password here>
spring.datasource.driver-class-name=com.mysql.jdbc.Driver
spring.jpa.database-platform=org.hibernate.dialect.MySQL5InnoDBDialect
```

Create Docker image and run as docker container

```
$ cd /opt/fittr/source
$ git clone git@bitbucket.org:community_developers/dmf0420pd017-squatsfitness-java-contactservice.git -b develop contactservice
$ cd contactservice && mvn clean install
$ docker build -t contactservice .
$ sudo docker run -d --name=contactservice -e JAVA_OPTS='-Xmx512m -Xmx512m' -v /var/logs/fittr/contactservice/:/var/logs/fittr/contactservice/ -v /opt/fittr/config/:/opt/fittr/config/ --network=fittr contactservice
```


#### 5.2.6. Media Service:

Create *application.properties* file in resource folder and add details for the data source:

```
spring.datasource.url=jdbc:mysql://localhost/<your db name here>
spring.datasource.username=<your db username here>
spring.datasource.password=<your db password here>
spring.datasource.driver-class-name=com.mysql.jdbc.Driver
spring.jpa.database-platform=org.hibernate.dialect.MySQL5InnoDBDialect
```

Create Docker image and run as docker container

```
$ cd /opt/fittr/source
$ git clone git@bitbucket.org:community_developers/dmf0420pd017-squatsfitness-java-mediaservice.git -b develop mediaservice
$ cd mediaservice && mvn clean install
$ docker build -t mediaservice .
$ sudo docker run -d --name=mediaservice -e JAVA_OPTS='-Xmx512m -Xmx512m' -v /var/logs/fittr/mediaservice/:/var/logs/fittr/mediaservice/ -v /opt/fittr/config/:/opt/fittr/config/ --network=fittr mediaservice
```


#### 5.2.7. XMPP Service:

Create *application.properties* file in resource folder and add details for the data source:

```
spring.datasource.url=jdbc:mysql://localhost/<your db name here>
spring.datasource.username=<your db username here>
spring.datasource.password=<your db password here>
spring.datasource.driver-class-name=com.mysql.jdbc.Driver
spring.jpa.database-platform=org.hibernate.dialect.MySQL5InnoDBDialect
```

Create Docker image and run as docker container

```
$ cd /opt/fittr/source
$ git clone git@bitbucket.org:community_developers/dmf0420pd017-squatsfitness-java-xmppservice.git -b develop xmppservice
$ cd xmppservice && mvn clean install
$ docker build -t xmppservice .
$ sudo docker run -d --name=xmppservice -e JAVA_OPTS='-Xmx512m -Xmx512m' -v /var/logs/fittr/xmppservice/:/var/logs/fittr/xmppservice/ -v /opt/fittr/config/:/opt/fittr/config/ --network=fittr xmppservice
```


#### 5.2.7. Notification service:

Create *application.properties* file in resource folder and add details for the data source:

```
spring.datasource.url=jdbc:mysql://localhost/<your db name here>
spring.datasource.username=<your db username here>
spring.datasource.password=<your db password here>
spring.datasource.driver-class-name=com.mysql.jdbc.Driver
spring.jpa.database-platform=org.hibernate.dialect.MySQL5InnoDBDialect
```

Create Docker image and run as docker container

```
$ cd /opt/fittr/source
$ git clone git@bitbucket.org:community_developers/dmf0420pd017-squatsfitness-java-notificationservice.git -b develop notificationservice
$ cd notificationservice && mvn clean install
$ docker build -t notificationservice .
$ sudo docker run -d --name=notificationservice -e JAVA_OPTS='-Xmx256m -Xms256m'  -v /opt/fittr/config/voip.p12:/opt/fittr/config/voip.p12 -v /opt/fittr/config/apns.p12:/opt/fittr/config/apns.p12  -v /var/logs/fittr/notificationservice/:/var/logs/fittr/notificationservice/ --network=fittr notificationservice
```



### 5.3. Webadmin
requirepass
#### 5.3.1. Run the following command to pull from git repository


```
$ cd /opt/fittr/source
$ git clone git@bitbucket.org:community_developers/dmf0420pd017-squatsfitness-web-admin.git -b develop webadmin
```


#### 5.3.2. Create .env file and add the following

**Note: replace DB_PASSWORD with your mysql root password**


```
$ cd /opt/fittr/source/webadmin
$ vi .env
APP_ENV=production
APP_DEBUG=false
APP_KEY=base64:o0N3hEAhWaHV5DKAYAsQFjeJcPI3oDJofAMjlNmopIs=

DB_CONNECTION=mysql
DB_HOST=your ip
DB_PORT=3306
DB_DATABASE=ejabberd_19
DB_USERNAME=root
DB_PASSWORD=password

CACHE_DRIVER=file
SESSION_DRIVER=file
QUEUE_DRIVER=sync

MAIL_DRIVER=smtp
MAIL_HOST=smtp.gmail.com
MAIL_PORT=587
MAIL_USERNAME=infomirrorfly@contus.in
MAIL_PASSWORD=Feedback!@1234
MAIL_ENCRYPTION=tls

DOMAIN=contus.in
ADMIN_USER=123456789
ADMIN_PASSWORD=123456789
XMPP_ADDRESS=<xmpp domain>:5222
XMPP_DOMAIN=<xmpp domain>
XMPP_IP=<xmpp domain>
APP_URL=https://<webadmin domain name>

LOGIN_API_URL=https://<api domain name>/api/v1/login
IMAGE_DOWNLOAD_URL=https://<api domain name>/api/v1/media/
IMAGE_UPLOAD_URL=https://<api domain>/api/v1/media/
```


#### 5.3.3. Create Docker image and run as docker container


```
$ docker build -t webadmin .
$ docker run -d --name=webadmin -p 8000:80 webadmin
```


### 5.4. Signal Server

Create Docker image and run as docker container


```
$ cd /opt/fittr/source
$ git clone git@bitbucket.org:community_developers/dmf0420pd017-squatsfitness-signalserver.git -b develop signalserver
$ cd signalserver

(## create .env file and add port)
$ vi .env1 
PORT=3001
HTTP_REQUIRED=false
REDIS_HOST=your ip
REDIS_PORT=6379
REDIS_PASSWORD=your password
```


#### 5.4.1. Create Docker image and run as docker container


```
$ docker build -t signalserver .
$ docker run -d --name=signalserver -p 3001:3001 signalserver
```


### 5.5. Webchat

#### 5.5.1. Run the following command to pull from git repository

**Install node and npm**

```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | zsh
source ~/.zshrc
nvm install lts/fermium
node -v
npm -v
```

**For sudo to work, create soft link to /usr/bin/npm**
```
sudo ln -s /home/lamtei/.nvm/versions/node/v14.18.2/bin/npm /usr/bin/npm
sudo ln -s /home/lamtei/.nvm/versions/node/v14.18.2/bin/node /usr/bin/node 
```

```
$ cd /opt/fittr/source
$ git clone https://username@bitbucket.org/Apptha/dmf0420pd017-squatsfitness-instantmessenger-web-chat.git -b develop webchat
```



```
$ (## create .env file and add port)
$ vi .env
 port=3000
SOCKETIO_SERVER_HOST = https://your  signalserver domain/
SOCKETIO_SERVER_PORT = 3001
XMPP_SERVER_HOST = your ejabberd domain name
XMPP_SOCKET_HOST = wss://your ejabberd domain name/websocket port
Adminuser = 123456789
IV = 'ddc0f15cc2c90fca'
API_URL = https://your api domain name/api/v1
Default_Status = I am in fittr
COMPANYDOMAIN=fittr.com
Recall_Time = 5
```



```
$ sudo npm install
$ sudo npm run build
$ npm start
```

**Run as docker service**
```
$ docker build -t webchat . 
$ docker run -d --name=webchat -p 3000:80 webchat 
```

## TODO: Following has not been tested in local

## 6. Stun And Turn Installation

[Coturn](https://github.com/coturn/coturn) is an open source turn server. We can easily setup Coturn on Ubuntu 18.04 with the official Coturn repo.

**Step 1: Firewall Rules**

First Make sure that you have opened up the following ports in your firewall.


```
3478 : UDP
16384-32768 : UDP
5349 : UDP
```


**Step 2: Installing Coturn**

Login to Ubuntu shell and enter the following command to install Coturn


```
sudo apt-get -y update
sudo apt-get -y install coturn
```


**Step 3: Enable TURN Server**


```
sudo vim /etc/default/coturn
```


Uncomment the following line by removing the # at the beginning to run Coturn as an automatic system service daemon.


```
TURNSERVER_ENABLED=1
```


**Step 4:  Creating a User With the Configuration File**

This method should work with most of the versions of Coturn. Open (or create) **/etc/turnserver.conf **file and past the following content. Replace &lt;YOUR_USERNAME>, &lt;YOUR_PASSWORD> and &lt;YOUR_PUBLIC_IP_ADDRESS> values with your own ones.


```
fingerprint
user=<YOUR_USERNAME>:<YOUR_PASSWORD>
lt-cred-mech
realm=example.com
log-file=/var/log/turnserver/turnserver.log
cert=/etc/ssl/example.pem
pkey=/etc/ssl/private.key

simple-log
external-ip=<YOUR_PUBLIC_IP_ADDRESS>
```


**Step 5: restart the coturn service.**


```
sudo service coturn restart
```


**Step 6 : (Optional) With Turn Admin (Quick and Easy)**

By default, coturn is configured to use an sqlite database. We can create a user in the database using the utility called turnadmin that comes with coturn.

To create a user with turnadmin use following command


```
turnadmin -k -u <YOUR_USERNAME> -p <YOUR_PASSWORD> -r <REALM>
```


For an example

turnadmin -k -u username -p 1234 -r stun.contus.us

**Step 7: Testing**

Go to [trickle-ice page](https://webrtc.github.io/samples/src/content/peerconnection/trickle-ice/) and enter the following details.


```
STUN or TURN URI : turn:<YOUR_PUBLIC_IP_ADDRESS>:3478
TURN username: <YOUR_USERNAME>
TURN password: <YOUR_PASSWORD>
```



## 7. Conference Group Call Installation

### 7.1. Server configuration

Choose Compute Optimized instances for better performance.


```
instance type: c5.xlarge
OS type: ubuntu
storage: 30GB
```


### 7.2. Firewall Rules

First Make sure that you have opened up the following ports in your firewall.


```
10000 : UDP
80    : TCP
443   : TCP
4443  : TCP
5222  : TCP
5347  : TCP
```


### 7.3. Installation

      

#### 7.3.1. Install required repositories**


```
sudo apt update
sudo apt install apt-transport-https
sudo apt-add-repository universe
```


**     **

#### 7.3.2. Changing your hostname


```
sudo hostnamectl set-hostname <domain-name>
```


* Change &lt;domain-name> with your Domain name

 

Edit the /etc/hosts file and add the domain name


```
$ nano /etc/hosts
127.0.0.1 localhost <domain-name>
```


* Change &lt;domain-name> with your Domain name

#### 7.3.3. Add the package repo and update


```
$ curl https://download.jitsi.org/jitsi-key.gpg.key | sudo sh -c 'gpg --dearmor > /usr/share/keyrings/jitsi-keyring.gpg'
$ echo 'deb [signed-by=/usr/share/keyrings/jitsi-keyring.gpg] https://download.jitsi.org stable/' | sudo tee /etc/apt/sources.list.d/jitsi-stable.list > /dev/null
```



```
sudo apt update
```


**      ** 
#### 7.3.4. Install Jitsi Meet


```
sudo apt install jitsi-meet
```


You will be asked about SSL certificate generation. Keep the default answer, "Generate a new self-signed certificate" unless you opt not to use an SSL certificate from LetsEncrypt.

**Generate a Let's Encrypt certificate**


```
sudo /usr/share/jitsi-meet/scripts/install-letsencrypt-cert.sh
```


To update the values edit /etc/systemd/system.conf and make sure you have the following values if values are smaller, if not do not update;


```
$nano /etc/systemd/system.conf
DefaultLimitNOFILE=65000
DefaultLimitNPROC=65000
DefaultTasksMax=65000
```


#### 7.3.5. Restart the services

 


```
$ service nginx restart
$ service prosody restart
$ service jitsi-videobridge2 restart
$ service jicofo restart
```


** **

#### 7.3.6. check the services status

  


```
$ service nginx status
$ service prosody status
$ service jitsi-videobridge2 status
$ service jicofo status
```


   

To verify the installation open a browser and type the domain name.Now provide a group name and continue.


## 8. Jitsi Meet with multiple Videobridge nodes

### 8.1. Prerequisites:

1.Ubuntu machine with Default Jitsi Meet installation (Server A)

2.Ubuntu machine (Server B)

### 8.2. Configuration Change to Server A

#### 8.2.1. Add the JIDs of the Videobridges as Admins

In Prosody configuration **/etc/prosody/conf.d/&lt;dnsname>.cfg.lua**, add the following line:


```
$ vi /etc/prosody/conf.d/<dnsname>.cfg.lua

VirtualHost "<dnsname>"
  -- enabled = false -- Remove this line to enable this host
  authentication = "anonymous"
  admins = {
    "jitsi-videobridge.<dnsname>",
    "videobridge2.<dnsname>",
}
```


#### 8.2.2. Configure Videobridge configuration

Edit the videobridge configuration in **/etc/jitsi/videobridge/config**, and look for the configuration line JVB_OPTS="--apis=,", and change it to following.


```
$ vi /etc/jitsi/videobridge/config

JVB_OPTS="--apis=rest,xmpp --subdomain=jitsi-videobridge"
JVB_HOST=<domainname> 
```


#### 8.2.3. Change Prosody to listen to public interface

Edit the prosody configuration /etc/prosody/prosody.cfg.lua , add the following configuration under **admin = { }**


```
$ vi /etc/prosody/prosody.cfg.lua

component_ports = {5347}
component_interface = "0.0.0.0"
```


#### 8.2.4. Change Jicofo configuration to use public domain

Change the following configuration files to replace localhost with your jitsi domain.


```
$ vi /etc/jitsi/jicofo/config

JICOFO_HOST=<domainname>  //domain name is the domain name of your jitsi server (Server A)
```


#### 8.2.5. Change Videobridge to use pubsub

To enable statistics and to set statistics to use pubsub, in **/etc/jitsi/videobridge/sip-communicator.properties **file** **add the following lines if they are missing, or otherwise make sure the transport is pubsub


```
$ vi /etc/jitsi/videobridge/sip-communicator.properties

org.jitsi.videobridge.ENABLE_STATISTICS=true
org.jitsi.videobridge.STATISTICS_TRANSPORT=pubsub
org.jitsi.videobridge.PUBSUB_SERVICE=<domainname>
org.jitsi.videobridge.PUBSUB_NODE=sharedStatsNode
```


#### 8.2.6. Change Jicofo to use pubsub

In** /etc/jitsi/jicofo/sip-communicator.properties** file, adding the following lines


```
vi /etc/jitsi/jicofo/sip-communicator.properties

org.jitsi.focus.pubsub.ADDRESS=<domainname>
org.jitsi.focus.STATS_PUBSUB_NODE=sharedStatsNode
```


#### 8.2.7. Open Ports on the main server (Server A)

Make sure the following ports are open:


```
TCP/UDP Port 5347
TCP/UDP Port 5222
```


### 8.3. Changes on the second videobridge server (Server B)

**  **

#### 8.3.1. Add the Jitsi package library


```
curl https://download.jitsi.org/jitsi-key.gpg.key | sudo sh -c 'gpg --dearmor > /usr/share/keyrings/jitsi-keyring.gpg'
echo 'deb [signed-by=/usr/share/keyrings/jitsi-keyring.gpg] https://download.jitsi.org stable/' | sudo tee /etc/apt/sources.list.d/jitsi-stable.list > /dev/null
```


#### 8.3.2. Install jitsi videobridge


```
apt update
apt -y install jitsi-videobridge2
```


#### 8.3.3 Configure Videobridge to use pubsub

To enable statistics and to set statistics to use pubsub, in **/etc/jitsi/videobridge/sip-communicator.properties **file** **make the following changes. Add the following lines if they are missing, or otherwise if STATISTIC_TRANSPORT is set to muc, change it to pubsub. Make sure the domain name is pointing to server A.


```
$ /etc/jitsi/videobridge/sip-communicator.properties

org.jitsi.videobridge.ENABLE_STATISTICS=true
org.jitsi.videobridge.STATISTICS_TRANSPORT=pubsub
org.jitsi.videobridge.PUBSUB_SERVICE=<domainname>
org.jitsi.videobridge.PUBSUB_NODE=sharedStatsNode
```


#### 8.3.4. Configure Videobridge to connect to the main server

Edit the videobridge configuration in **/etc/jitsi/videobridge/config**, and look for the configuration line JVB_OPTS="--apis=,", and change it to following.


```
$ vi /etc/jitsi/videobridge/config

JVB_OPTS="--apis=rest,xmpp --subdomain=videobridge2" 
JVB_HOST=<domainname> 
```


By default, the JID uses jitsi-videobridge as the default subdomain, so in this second node, we set the subdomain as videobridge2.

### 8.4. Add new videobridge components to main Jitsi server (Server A)

#### 8.4.1. Set videobridge credentials

Edit the file in /etc/prosody/conf.d/&lt;dnsname>.cfg.lua, at the bottom of the main VirtualHost add the following


```
$ vi /etc/prosody/conf.d/<dnsname>.cfg.lua

Component "videobridge2.<dnsname>" //This is the domain name of the second videobridge
   component_secret = "<password>" //This can be found on second VB, under /etc/jitsi/videobridge/config
```


### 8.5. Restart the services

#### 8.5.1. Restart the services on Server A


```
$ sudo service prosody restart
$ sudo service jicofo restart
$ sudo service jitsi-videobridge2 restart
$ sudo service nginx restart
```


#### 8.5.2. Restart videobridge on Server B


```
$ service jitsi-videobridge2 restart
```


### 8.6. Check the logs

#### 8.6.1. Check the following log files on Server A


```
$ tail -f /var/log/jitsi/jicofo.log
$ tail -f /var/log/jitsi/jvb.log
$ tail -f /var/log/prosody/prosody.log
```


#### 8.6.2. Check the following log files on Server B

```
$ tail -f /var/log/jitsi/jvb.log
```


## 9. Troubleshooting


### 9.1. Troubleshooting – API and Dashboard

**Check open ports in case of conflict**
```
$ sudo lsof -i -P -n | grep LISTEN  
```
In case Jitsi conflicts with other ports, you can temporarily disable Jitsi
```
$ systemctl stop jitsi-videobridge2
```

**Ejabberd source**

```
cd /opt/ejabberd/source/ejabberd
```

**Docker container config files**

```
cd /opt/fittr/config/
```

The conf .yml files are as below


**1. gatewayservice.yml**

Edit the gatewayservice.yml file with the redis server IP, MySQL server IP followed by database name

**1. mediaservice.yml**

Edit the mediaservice.yml file with the MySQL server IP followed by database name.

**3. contactservice.yml**

Edit the contactservice.yml file with the MySQL server IP followed by database name.

**4. userservice.yml**

Edit the userservice.yml file with the MySQL server IP,Redis details followed by database name and RabbitMQ domain

**5. xmppservice.yml**

Edit the xmppservice.yml file with the xmpphost and domain followed by  RabbitMQ domain

**6. notifictionservice.yml**

Edit the notificationservice.yml file with the p12.key followed by  RabbitMQ domain


**Check docker container source**

```
cd /opt/fittr/source/gatewayservice/
cd /opt/fittr/source/configservice/
cd /opt/fittr/source/mediaservice/
cd /opt/fittr/source/contactservice/
cd /opt/fittr/source/userservice/
cd /opt/fittr/source/xmppservice/
cd /opt/fittr/source/notificationservice/
cd /opt/fittr/source/webamin/
cd /opt/fittr/source/webchat/
```

**Check the logs located in**


The log file for Ejabberd will be stored under ejabberd user.

Move to the corresponding directory /opt/ejabberd and run below command,

tail -f var/log/ejabberd/ejabberd.log


For the docker container logs kindly use the below commands,

```
tail -f /var/logs/fittr/gatewayservice/gatewayservice.log
tail -f /var/logs/fittr/configservice/configservice.log
tail -f /var/logs/fittr/mediaservice/mediaservice.log
tail -f /var/logs/fittr/contactservice/contactservice.log
tail -f /var/logs/fittr/userservice/userservice.log
tail -f /var/logs/fittr/xmppservice/xmppervice.log
tail -f /var/logs/fittr/notificationservice/notificationservice.log
tail -f /var/log/jitsi/jicofo.log
tail -f /var/log/jitsi/jvb.log
```

The log file for conference group call:

```
tail -f /var/log/jitsi/jicofo.log
tail -f /var/log/jitsi/jvb.log
```