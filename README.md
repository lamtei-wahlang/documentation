## Project setup for Ubuntu based distributions

* Please get necessary access to the repositories. For the current project, the repositories can be found [here](https://bitbucket.org/community_developers/workspace/projects/COMMUNITY_SQUATS).
* Clone the required repositories to your local. 
* Check [Setup.md](./Setup.md) for the documentation on setting up your local environment.
* This document is to guide the setup details for Ubuntu based distributions. For Cent OS, you can follow the existing documentation in Production.

#### Related Links:
* [MirrorFly setup documentation](https://docs-internal.mirrorfly.com/docs/mysql)
* [MirrorFly services](https://drive.google.com/file/d/1bao6wjLb7L0ee4xnCJR2kGybL4drDY7N/view)
* [MirrorFly schema](https://docs.google.com/document/d/1IQQJRCUMQ03xiLRD138Pbe5EHUu5228sVltTo-vtj5A/edit#heading=h.pltzxbrqeqq2)
* [FITTR setup documentation](https://docs.google.com/document/d/1EauQsWvVf5xe9btjPMPMDqODchAfJqpDPm1fipC63Lc/)